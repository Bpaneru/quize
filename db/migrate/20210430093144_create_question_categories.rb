# frozen_string_literal: true

class CreateQuestionCategories < ActiveRecord::Migration[6.1]
  def change
    create_table :question_categories do |t|
      t.references :category, type: :bigint, index: true
      t.references :question, type: :bigint, index: true
      t.timestamps
    end
  end
end
