# frozen_string_literal: true

class CreateQuizQuestions < ActiveRecord::Migration[6.1]
  def change
    create_table :quiz_questions do |t|
      t.references :quiz, type: :bigint, index: true
      t.references :question, type: :bigint, index: true
      t.bigint :given_answer
      t.timestamps
    end
  end
end
