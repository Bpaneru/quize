# frozen_string_literal: true

class CreateAnswers < ActiveRecord::Migration[6.1]
  def change
    create_table :answers do |t|
      t.text :answer, null: false
      t.boolean :correct, null: false, default: false
      t.references :question, type: :bigint, index: true, foreign_key: true

      t.timestamps
    end
  end
end
