# frozen_string_literal: true

class CreateQuizzes < ActiveRecord::Migration[6.1]
  def change
    create_table :quizzes do |t|
      t.string :name, null: false
      t.integer :total_question
      t.integer :total_correct_answer, default: 0
      t.float :percentage, null: true
      t.bigint :user_id, null: true
      t.timestamps
    end
  end
end
