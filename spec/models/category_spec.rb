# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Category, type: :model do
  Category.create(name: 'test1')
  it 'is valid with valid attributes' do
    expect(Category.new(name: 'test')).to be_valid
  end

  it 'is not valid without a name' do
    category = Category.new(name: nil)
    expect(category).to_not be_valid
  end

  it 'is not valid if name is not unique' do
    category = Category.new(name: 'test1')
    expect(category).to_not be_valid
  end

  it 'is not valid if name has less then 3 character' do
    category = Category.new(name: 'aa')
    expect(category).to_not be_valid
  end

  it 'is not valid if name has more then 25 character' do
    category = User.new(name: 'abdendnjendhfjedndhndjdndjdhd')
    expect(category).to_not be_valid
  end
end
