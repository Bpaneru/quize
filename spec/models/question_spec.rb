# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Question, type: :model do
  Question.create(question: 'test question?',
                  answers_attributes: [{ answer: 'test answer1', correct: false },
                                       { answer: 'test answer2', correct: true }, { answer: 'test answer3', correct: false }])
  it 'is valid with valid attributes' do
    expect(Question.new(question: 'test question1?',
                        answers_attributes: [{ answer: 'test answer1.1', correct: false }, { answer: 'test answer2.1', correct: true },
                                             { answer: 'test answer3.1', correct: false }])).to be_valid
  end

  it 'is not valid without a question' do
    question = Question.new(question: nil)
    expect(question).to_not be_valid
  end

  it 'is not valid without a answer' do
    question = Question.new(question: 'invalid answer?')
    expect(question).to_not be_valid
  end

  it 'is not valid if question is not unique' do
    question = Question.create(question: 'test question?',
                               answers_attributes: [{ answer: 'test answer1.1', correct: false }, { answer: 'test answer2.1', correct: true },
                                                    { answer: 'test answer3.1', correct: false }])
    expect(question).to_not be_valid
  end

  it "is not valid if question have two or more right answers" do 
    question = Question.new(question: "test question1?", 
      answers_attributes: [{answer: "test answer1.1", correct: false},{answer: "test answer2.1", correct: true}, {answer: "test answer3.1", correct: true}])
    expect(question).to_not be_valid
  end

  it 'is not valid if question has less then 10 character' do
    question = Question.new(question: 'a?',
                            answers_attributes: [{ answer: 'test answer1.1', correct: false }, { answer: 'test answer2.1', correct: true },
                                                 { answer: 'test answer3.1', correct: false }])
    expect(question).to_not be_valid
  end

  it 'is not valid if answer has less then 3 character' do
    question = Question.new(question: 'test question1?',
                            answers_attributes: [{ answer: 'test answer1.1', correct: false }, { answer: 'test answer2.1', correct: true },
                                                 { answer: 'a', correct: false }])
    expect(question).to_not be_valid
  end
end
