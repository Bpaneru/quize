# frozen_string_literal: true

class QuestionPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope.all
    end
  end

  def update?
    user.admin?
  end

  def create?
    update?
  end

  def destroy?
    update?
  end
end
