# frozen_string_literal: true

class ReportMailer < ApplicationMailer
  def quiz_report(email, report, quiz)
    @email = email
    @results = report
    @quiz = quiz
    mail(to: @email, subject: 'Quiz Result')
  end
end
