# frozen_string_literal: true

class ApplicationMailer < ActionMailer::Base
  default from: 'result@binod-quiz.com'
  layout 'mailer'
end
