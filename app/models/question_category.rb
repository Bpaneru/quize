# frozen_string_literal: true

class QuestionCategory < ApplicationRecord
  belongs_to :category
  belongs_to :question
end
