# frozen_string_literal: true

class Question < ApplicationRecord
  has_many :question_categories
  has_many :categories, through: :question_categories
  has_many :answers
  has_many :quiz_questions
  has_many :quizzes, through: :quiz_questions
  accepts_nested_attributes_for :answers, allow_destroy: true, reject_if: :all_blank

  validates :question, presence: true, length: { minimum: 10, maximum: 200 }
  validates_uniqueness_of :question
  validate :answer_validation

  def answer_validation
    errors.add(:option, 'Please select only one correct answer') if answers.map(&:correct).count(true) != 1
  end
end
