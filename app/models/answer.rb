# frozen_string_literal: true

class Answer < ApplicationRecord
  belongs_to :question

  validates :answer, presence: true, length: { minimum: 3, maximum: 200 }
end
