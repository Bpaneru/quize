# frozen_string_literal: true

class Quiz < ApplicationRecord
  has_many :quiz_questions
  has_many :questions, through: :quiz_questions
  belongs_to :user, optional: true

  validates  :name,  presence: true
  validates_uniqueness_of :name
end
