# frozen_string_literal: true

class QuestionsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_question, only: %i[show edit update destroy]
  before_action :set_question_answers, only: %i[edit]

  def index
    @questions = Question.paginate(page: params[:page], per_page: 5)
  end

  def new
    @question = Question.new
    @answers = @question.answers.build
  end

  def create
    @question = Question.new(question_params)
    authorize @question
    if @question.save 
      flash[:notice] = "question was sucessfully created."
      redirect_to questions_path
    else 
      render 'new'
    end
  end

  def show; end

  def edit; end

  def update
    authorize @question
    if @question.update(question_params)
      flash[:notice] = 'Question was updated successfully'
      redirect_to @question
    else
      render 'edit'
    end
  end

  def destroy
    @question.destroy
    redirect_to questions_url
    flash[:notice] = 'Question sucessfully deleted.'
  rescue StandardError => e
    flash[:alert] = 'This question might have been used in quiz. '
    redirect_to questions_url
  else
  end

  private

  def question_params
    params.require(:question).permit(:question, category_ids: [],
                                                answers_attributes: %i[id answer correct _destroy])
  end

  def set_question
    @question = Question.find(params[:id])
  end

  def set_question_answers
    @answers = Question.find(params[:id]).answers
  end
end
