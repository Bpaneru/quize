# frozen_string_literal: true

class QuizzesController < ApplicationController
  before_action :set_quiz, only: [:create]
  before_action :authenticate_user!, only: [:result]

  def category_select
    render 'category_select'
  end

  def new
    if !params[:category_ids].empty?
      @guest_email = params[:email] if params[:email]
      @questions = []
      @questions.push(categories_question)
      create_quiz if @questions.flatten!
      @questions
      @quiz
    else
      flash[:error] = 'please select the category!'
      redirect_to category_select_path
    end
  end

  def create
    QuizQuestion.where(quiz_id: params[:quiz_id]).each do |quiz_question|
      puts "id is #{params["#{quiz_question.question_id}"]}"
       quiz_question.update(given_answer: params["#{quiz_question.question_id}"])
       if quiz_question.given_answer != nil
        correct_answer(quiz_question.given_answer)
       end
    end
    if overall
      quiz = @quiz
      send_email(set_email, result, quiz)
      result
      @quiz
      flash[:notice] = 'You have sucessfully attempted the quiz.'
      render 'result'
    end
  end

  def result
    @results = @quiz.quiz_questions
  end

  def send_email(email, result, quiz)
    quiz = @quiz
    ReportMailer.quiz_report(email, result, quiz).deliver
  end

  private

  def set_quiz
    @quiz = Quiz.find(params[:quiz_id])
  end

  def set_email
    if user_signed_in?
      current_user.email
    else
      params[:email]
    end
  end

  def correct_answer(id)
    if Answer.find(id).correct? 
      @total_correct_answer = @total_correct_answer.nil? ? 1 : @total_correct_answer + 1
    end
  end

  def overall
    @quiz.update(total_correct_answer: @total_correct_answer.nil? ? 0 : @total_correct_answer,
                 percentage: calculate_percentage.round(2))
  end

  def calculate_percentage
    @total_correct_answer.nil? ? @quiz.total_correct_answer : @total_correct_answer / @quiz.total_question.to_f * 100
  end

  def all_questions
    Question.all
  end

  def categories_question
    Category.find(params[:category_ids])&.questions
  end

  def create_quiz
    @quiz = Quiz.new
    @quiz.name =   "#{@questions.first.categories.first.name}-#{Time.now} "
    @quiz.total_question = @questions.count
    @quiz.user_id = current_user.id if current_user
    quiz_question(@quiz.id) if @quiz.save
  end

  def quiz_question(quiz_id)
    @questions.each do |question|
      QuizQuestion.create(quiz_id: quiz_id, question_id: question.id)
    end
  end
end
