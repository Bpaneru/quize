# frozen_string_literal: true

class AnswersController < ApplicationController
  before_action :authenticate_user!

  def new
    @answer = Answer.new
  end
end
