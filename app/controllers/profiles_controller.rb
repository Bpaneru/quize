# frozen_string_literal: true

class ProfilesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_quiz, only: [:show]

  def index
    @quizzes = Quiz.all.where(user_id: current_user.id).paginate(page: params[:page], per_page: 5)
  end

  def user_list
    @users = User.where(admin: false).paginate(page: params[:page], per_page: 5)
  end

  def user_quiz
    @quizzes = User.find(params[:id]).quizzes.paginate(page: params[:page], per_page: 5)
  end

  def show
    if @quiz.present?
      @results = @quiz.quiz_questions
      @quiz
    else
      flash[:notice] = "You havn't perfom a quiz yet!"
      render 'index'
    end
  end

  private

  def set_quiz
    @quiz = Quiz.find(params[:id])
  end
end
