# frozen_string_literal: true

Rails.application.routes.draw do
  devise_for :users
  resources :categories, only: %i[index new create update edit show]
  get 'category/all-questions/:id', to: 'categories#destroy'
  resources :questions, only: %i[index new create update edit show]
  get 'questions/:id', to: 'questions#destroy'
  root 'pages#index'
  get 'category_select', to: 'quizzes#category_select'
  get 'start', to: 'quizzes#new'
  get 'result', to: 'quizze#result'
  resources :quizzes, only: %i[new create update edit]


  resources :profiles, only: %i[index show]
  get 'users', to: 'profiles#user_list'
  get 'user/results/:id', to: 'profiles#user_quiz'


  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
